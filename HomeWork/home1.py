import cirq


def main():
    qubits = cirq.LineQubit.range(4)

    qc = cirq.Circuit()

    qc.append(cirq.X(qubits[3]).controlled_by(qubits[0], qubits[1], qubits[2]))

    oracle_circuit = qc

    main_circuit = cirq.Circuit()

    main_circuit.append(cirq.H.on_each(*cirq.LineQubit.range(4)))

    main_circuit.append(oracle_circuit)

    main_circuit.append(cirq.measure(qubits[3], key='result'))

    simulator = cirq.Simulator()

    result = simulator.run(main_circuit)

    print("Measurement result:", result.measurements['result'])


if __name__ == "__main__":
    main()
