import cirq


def grover_algorithm():
    qubits = cirq.LineQubit.range(3)

    circuit = cirq.Circuit()
    circuit.append(cirq.H.on_each(*qubits))

    circuit.append(cirq.X(qubits[1]))
    circuit.append(cirq.CCX(qubits[0], qubits[1], qubits[2]))
    circuit.append(cirq.X(qubits[1]))

    circuit.append(cirq.H.on_each(*qubits))

    circuit.append(cirq.X(qubits[0]))
    circuit.append(cirq.CCX(qubits[0], qubits[1], qubits[2]))
    circuit.append(cirq.X(qubits[0]))

    circuit.append(cirq.H.on_each(*qubits))

    circuit.append(cirq.measure(*qubits[:-1], key='result'))

    return circuit


def main():
    result = cirq.Simulator().run(grover_algorithm())
    print("Measurement result:", result.measurements['result'])


if __name__ == "__main__":
    main()
