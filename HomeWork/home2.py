import cirq


def main():
    qubits = cirq.LineQubit.range(4)

    main_circuit = cirq.Circuit()

    main_circuit.append(cirq.H.on_each(*qubits))

    oracle_qubits = cirq.LineQubit.range(4)

    oracle_circuit = cirq.Circuit()

    oracle_circuit.append(cirq.X(oracle_qubits[3]))

    print("Before Oracle:")
    print(cirq.final_state_vector(main_circuit + oracle_circuit, qubit_order=qubits))

    simulator = cirq.Simulator()

    oracle = oracle_circuit

    main_circuit.append(oracle)

    main_circuit.append(cirq.H.on_each(qubits[:-1]))

    print("After Oracle:")
    print(cirq.final_state_vector(main_circuit, qubit_order=qubits))

    main_circuit.append(cirq.measure(*qubits[:-1], key='result'))

    circuit = main_circuit

    result = simulator.run(circuit)

    print("Measurement result:", result.measurements['result'])


if __name__ == "__main__":
    main()
